import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

public class EncodedTextPrinter implements TextPrinter{
    @Override
    public void printText(String text) {
        System.out.println(Arrays.toString(Base64.getEncoder().encode(text.getBytes(StandardCharsets.UTF_8))));
    }
}

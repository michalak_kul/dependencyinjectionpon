import java.util.Locale;

public class UpperCaseTextPrinter implements TextPrinter {

    public void printText(String text) {
        System.out.println(text.toUpperCase(Locale.ROOT));
    }
}

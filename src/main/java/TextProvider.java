public interface TextProvider {

    String provideText();
}

public class Main {
    public static void main(String[] args) {

        TextProvider consoleTextProvider = new ConsoleTextProvider();
        TextPrinter upperCaseTextPrinter = new UpperCaseTextPrinter();
        TextPrinter lowerCaseTextPrinter = new LowerCaseTextPrinter();
        TextPrinter encodedTextPrinter = new EncodedTextPrinter();
        TextService textService = new TextService(consoleTextProvider, upperCaseTextPrinter);
        textService.processText();

    }
}

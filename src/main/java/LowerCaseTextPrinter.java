public class LowerCaseTextPrinter implements TextPrinter{
    @Override
    public void printText(String text) {
        System.out.println(text.toLowerCase());
    }
}

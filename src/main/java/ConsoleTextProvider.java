import java.util.Scanner;

public class ConsoleTextProvider implements TextProvider{
    @Override
    public String provideText() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz jakis tekst");
        return scanner.nextLine();
    }
}

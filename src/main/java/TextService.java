public class TextService {

    private final TextProvider textProvider;
    private final TextPrinter textPrinter;


    public TextService(TextProvider textProvider, TextPrinter textPrinter) {
        this.textProvider = textProvider;
        this.textPrinter = textPrinter;
    }

    public void processText(){
        String text = textProvider.provideText();
        textPrinter.printText(text);
    }
}
